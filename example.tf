# Test config for creating an AWS server instance, running specified python 
# script(s), returning any and all outputs and destroying the instance.
# Creating instance, running scripts, getting output(s) and destroying instance(s)
# will be handled in a python application. This script will create the instance(s) 
# based on the parameters provided by the main python script.


provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = "${aws_default_vpc.default.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]# add a CIDR block here
  }

  egress {
    from_port    = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
}

#instance ami and type declaration. (AMI => Amazon Machine Image) 

resource "aws_instance" "script_runner" {
  # dynamic lookup in the map "{lookup(var.amis, var.region)}". can use ${var.amis["us-east-1"]} for static lookup
  ami           = "${var.amis["us-east-1"]}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  security_groups = ["allow_ssh"]
  depends_on = ["aws_security_group.allow_ssh"]

  # provisioner "local-exec" {
  #   command = "echo ${aws_instance.script_runner.public_ip} > ip_address.txt"
  # }
  
  connection {
	type = "ssh"
	user = "ubuntu"
    private_key = "${file("RTkey.pem")}"
	agent = "false"
	timeout = "30s"
  }
  
  provisioner "remote-exec" {
    inline = ["mkdir scripts"]
  }
  
  provisioner "file" {
    source = "${var.file_name}"
    destination = "~/scripts/${var.file_name}"
  }
  
  provisioner "remote-exec" {
    inline = ["cd scripts", "chmod a+x ${var.file_name}", "python3 ./${var.file_name}"]
  }
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}
