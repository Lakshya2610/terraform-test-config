variable "access_key" {}
variable "secret_key" {}
variable "key_name" {}

variable "file_name" {}

variable "region" {
  default = "us-east-1"
}

variable "amis" {
  type = "map"
}
